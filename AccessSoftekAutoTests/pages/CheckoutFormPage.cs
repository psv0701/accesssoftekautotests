﻿using System;
using AccessSoftekAutoTests.util;
using OpenQA.Selenium;

namespace AccessSoftekAutoTests.pages
{
    public class CheckoutFormPage
    {
        //########## Setup ############
        private IWebDriver driver = null;
        private Util util = null;
        public CheckoutFormPage(IWebDriver d)
        {
            this.driver = d;
            util = new Util(d);
        }
        private string url = "https://qaautomation-test.s3-eu-west-1.amazonaws.com/index.html";
        private string title = "Checkout example";

        //########### Error Messages Value #############
        private string messageLastName = "Valid last name is required.";
        private string messageFirstName = "Valid first name is required.";
        private string messageNameOnCard = "Name on card is required";
        private string messageCreditCardNumber = "Credit card number is required";
        private string messageExpiration = "Expiration date required";
        private string messageCvv = "Security code required";
        private string messageOrderSuccess = "Your order was placed. Thank you for purchasing.";

        //########### Cart Elements Definition #############
        private By loading = By.XPath("//li[@id='loading']");
        private By quantity = By.XPath("//span[@class='badge badge-secondary badge-pill']");
        private By products = By.XPath("//ul[@id='itemsList']/li");
        private By totalAmount = By.XPath("//strong[@id='totalAmount']");
        private By redeenButton = By.XPath("//button[@class='btn btn-secondary']");
        private By orderSuccess = By.XPath("//div[@id='success']/h2");

        //########### Fields Element Definition #############
        private By firstNameInputField = By.XPath("//input[@id='firstName']");
        private By lastNameInputField = By.XPath("//input[@id='lastName']");
        private By nameOnCardInputField = By.CssSelector("#cc-name");
        private By creditCardNumberInputField = By.CssSelector("#cc-number");
        private By expirationInputField = By.CssSelector("#cc-expiration");
        private By cvvInputField = By.CssSelector("#cc-cvv");
        private By promoCodeInputField = By.CssSelector("#promoCode");
        private By continueToCheckoutButton = By.XPath("//button[@class='btn btn-primary btn-lg btn-block']");

        private By errorValidFirstName = By.XPath("(//input[@id='firstName']/following::div[@class=\"invalid-feedback\"])[1]");
        private By errorValidLastName = By.XPath("(//input[@id='lastName']/following::div[@class=\"invalid-feedback\"])[1]");
        private By errorValidNameOnCard = By.XPath("(//input[@id='cc-name']/following::div[@class=\"invalid-feedback\"])[1]");
        private By errorValidCardNumber = By.XPath("(//input[@id='cc-number']/following::div[@class=\"invalid-feedback\"])[1]");
        private By errorValidExpiration = By.XPath("(//input[@id='cc-expiration']/following::div[@class=\"invalid-feedback\"])[1]");
        private By errorValidCvv = By.XPath("(//input[@id='cc-cvv']/following::div[@class=\"invalid-feedback\"])[1]");

        private By getprestaShop = By.CssSelector(".popup-link.prestashop-link.primary-link");
        private By productMenuXpath = By.XPath("//*[@id='header-menu']/ul/div[1]/div[1]/a");
        private By featureMenuXpath = By.XPath("//*[@id='more-submenus-column-4093']/ul/li[3]/a");

        //######### Function Definition #################
        public bool pageIsLoaded()
        {
            return util.pageIsOpened(title);
        }

        //######### Enter Field #################
        public bool enterFirstName(string value)
        {
            return util.EnterValue(firstNameInputField, value);
        }
        public bool enterLastName(string value)
        {
            return util.EnterValue(lastNameInputField, value);
        }
        public bool enterNameOnCard(string value)
        {
            return util.EnterValue(nameOnCardInputField, value);
        }
        public bool enterCreditCardNumber(string value)
        {
            return util.EnterValue(creditCardNumberInputField, value);
        }
        public bool enterExpiration(string value)
        {
            return util.EnterValue(expirationInputField, value);
        }
        public bool enterCvv(string value)
        {
            return util.EnterValue(cvvInputField, value);
        }
        public bool enterPromoCode(string value)
        {
            return util.EnterValue(promoCodeInputField, value);
        }

        //#########  Check Error`s Message #################
        public bool errorMessageLastName()
        {
            return util.GetText(errorValidLastName).Contains(messageLastName);
        }
        public bool errorMessageFirstName()
        {
            return util.GetText(errorValidFirstName).Contains(messageFirstName);
        }
        public bool errorMessageNameOnCard()
        {
            return util.GetText(errorValidNameOnCard).Contains(messageNameOnCard);
        }
        public bool errorMessageCreditCardNumber()
        {
            return util.GetText(errorValidCardNumber).Contains(messageCreditCardNumber);
        }
        public bool errorMessageExpiration()
        {
            return util.GetText(errorValidExpiration).Contains(messageExpiration);
        }
        public bool orderSuccessMessage()
        {
            return util.GetText(orderSuccess).Equals(messageOrderSuccess);
        }
        public bool errorMessageCvv()
        {
            return util.GetText(errorValidCvv).Contains(messageCvv);
        }
        public bool cartIsLoaded()
        {
            return util.IsElementHide(loading);
        }
        public string quantityOfGoods()
        {
            string num = "";
            try
            {
                util.IsElementVisible(quantity);
                num = driver.FindElement(quantity).Text;
            }
            catch(NoSuchElementException e){
                Console.WriteLine("Element " + quantity + "not found on page " + driver.Title);
                return num;
            }
            return num;
        }
        public bool countOfProductsMoreThanOne()
        {
            try
            {
                util.IsElementVisible(products);
                int productsCount = driver.FindElements(products).Count;
                if(productsCount >= 2)
                {
                    return true;
                }
                return false;
            }
            catch (NoSuchElementException e)
            {
                Console.WriteLine("Products not found on page " + driver.Title);
                return false;
            }
        }
        public double getTotalAmountValue()
        {
            double totalDouble = 0;
            
            try
            {
                util.IsElementVisible(totalAmount);
                string totalStr = driver.FindElement(totalAmount).Text.Replace(".",",");
                try
                {
                    return totalDouble = Convert.ToDouble(totalStr);                   
                }
                catch (FormatException)
                {
                    Console.WriteLine("Unable to convert to a Double.", totalStr);
                }
                catch (OverflowException)
                {
                    Console.WriteLine("'{0}' is outside the range of a Double.", totalStr);
                }
            }
            catch (NoSuchElementException e)
            {
                Console.WriteLine("Element " + totalAmount + "not found on page " + driver.Title);
                return totalDouble;
            }
            return totalDouble;
        }

        public bool clickContinueToCheckoutButton()
        {
            return util.ClickElement(continueToCheckoutButton);
        }
        public bool clickRedeemButton()
        {
            return util.ClickElement(redeenButton);
        }
        public bool checkCorrectSale(double total, double totalWithPromo)
        {
            double rezult = totalWithPromo * 100 / total;
            if (rezult == 95)
            {
                return true;
            }
            Console.WriteLine("Promo code not correctly changes ‘Total’ field " + driver.Title);
            return false;
        }
        public void openCheckoutPage()
        {
            driver.Navigate().GoToUrl(url);
            driver.Manage().Window.FullScreen();
        }

    }
}
