﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace AccessSoftekAutoTests.RestSharpDataDriven.DataTypes
{
    public class DataResponseLengthNotEquals
    {
        [JsonProperty("success")]
        public bool Success { get; set; }
        [JsonProperty("event")]
        public object Event { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }
    }
}
