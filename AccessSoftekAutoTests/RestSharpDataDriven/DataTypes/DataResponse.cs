﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace AccessSoftekAutoTests.RestSharpDataDriven.DataTypes
{
    public class DataResponse
    {
        [JsonProperty("discount")]
        public int Discount { get; set; }
    }
}
