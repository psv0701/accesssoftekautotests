﻿using System;
using AccessSoftekAutoTests.RestSharpDataDriven.DataTypes;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using RestSharp;
using RestSharp.Serialization.Json;

namespace AccessSoftekAutoTests.tests
{
    public class ApiTestLengthNotEquals
    {
        private const string BASE_URL = "https://fg1ap986e9.execute-api.eu-west-1.amazonaws.com/Dev";

        [TestCase("11112222333344445", "Invalid Card Number", false ,TestName = "API returns error when credit card number length not equals 16")]
        [TestCase("1111222233334444", "", true, TestName = "API not returns error when credit card number length equals 16")]

        public void DataFor(string number, string expectedMessage, bool expectedStatus)
        {
            JObject jObjectbody = new JObject();
            jObjectbody.Add("firstName", "firstName");
            jObjectbody.Add("lastName", "lastName");
            jObjectbody.Add("paymentMethod", "paymentMethod");
            jObjectbody.Add("cc-name", "ccname");
            jObjectbody.Add("cc-number", $"{number}");
            jObjectbody.Add("cc-expiration", "01/20");
            jObjectbody.Add("cc-cvv", "123");

            // arrange
            RestClient client = new RestClient(BASE_URL);
            RestRequest request = new RestRequest($"/checkout", Method.POST);
            request.AddParameter("application/json", jObjectbody, ParameterType.RequestBody);

            // act
            IRestResponse response = client.Execute(request);
            DataResponseLengthNotEquals dataResponse = new JsonDeserializer().Deserialize<DataResponseLengthNotEquals>(response);

            // assert
            if (expectedStatus)
            {
                Assert.That(dataResponse.Success, Is.True);
            }
            else
            {
                Assert.That(dataResponse.Success, Is.False);
            }
            if (expectedStatus)
            {
                Assert.That(dataResponse.Message, Is.Null);
            }
            else
            {
                Assert.That(dataResponse.Message, Is.EqualTo(expectedMessage));
            }
        }
    }
}
