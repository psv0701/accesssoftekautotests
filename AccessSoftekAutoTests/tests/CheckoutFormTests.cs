﻿using System;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Chrome;
using System.Threading;
using AccessSoftekAutoTests.pages;

namespace AccessSoftekAutoTests.tests
{
    [TestFixture]
    public class CheckoutFormTests
    {
        IWebDriver driver;
        CheckoutFormPage checkoutFormPage = null;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            driver = new ChromeDriver();
            checkoutFormPage = new CheckoutFormPage(driver);
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            driver.Quit();
        }

        //"All mandatory empty fields should have error description displayed after ‘Continue to checkout’ is pressed"
        [Test, Description("All fields is epty")]
        public void AllFieldsIsEmpty()
        {
            checkoutFormPage.openCheckoutPage();
            checkoutFormPage.pageIsLoaded();
            checkoutFormPage.clickContinueToCheckoutButton();
            Assert.Multiple(() =>
            {
                Assert.IsTrue(checkoutFormPage.errorMessageLastName(), "LastName error Message is not showing");
                Assert.IsTrue(checkoutFormPage.errorMessageFirstName(), "FirstName error Message is not showing");
                Assert.IsTrue(checkoutFormPage.errorMessageNameOnCard(), "NameOnCard error Message is not showing");
                Assert.IsTrue(checkoutFormPage.errorMessageCreditCardNumber(), "CreditCardNumber error Message is not showing");
                Assert.IsTrue(checkoutFormPage.errorMessageCvv(), "Cvv error Message is not showing");
                Assert.IsTrue(checkoutFormPage.errorMessageExpiration(), "Expiration error Message is not showing");
            });
            Console.WriteLine("All fields is epty - testing completed");
        }

        [Test, Description("All fields is epty excluding First Name")]
        public void SomeFieldsIsEmptyExcludingFirstName()
        {
            checkoutFormPage.openCheckoutPage();
            checkoutFormPage.pageIsLoaded();
            checkoutFormPage.enterFirstName("FirstName");
            checkoutFormPage.clickContinueToCheckoutButton();
            Assert.Multiple(() =>
            {
                Assert.IsTrue(checkoutFormPage.errorMessageLastName(), "LastName error Message is not showing");
                Assert.IsFalse(checkoutFormPage.errorMessageFirstName(), "FirstName error Message is showing");
                Assert.IsTrue(checkoutFormPage.errorMessageNameOnCard(), "NameOnCard error Message is not showing");
                Assert.IsTrue(checkoutFormPage.errorMessageCreditCardNumber(), "CreditCardNumber error Message is not showing");
                Assert.IsTrue(checkoutFormPage.errorMessageCvv(), "Cvv error Message is not showing");
                Assert.IsTrue(checkoutFormPage.errorMessageExpiration(), "Expiration error Message is not showing");
            });
            Console.WriteLine("All fields is epty excluding First Name - testing completed");
        }

        [Test, Description("Some fields is epty excluding: First Name, Last Name")]
        public void SomeFieldsIsEmptyExcludingFirstNameLastName()
        {
            checkoutFormPage.openCheckoutPage();
            checkoutFormPage.pageIsLoaded();
            checkoutFormPage.enterFirstName("FirstName");
            checkoutFormPage.enterLastName("LastName");
            checkoutFormPage.clickContinueToCheckoutButton();
            Assert.Multiple(() =>
            {
                Assert.IsFalse(checkoutFormPage.errorMessageLastName(), "LastName error Message is showing");
                Assert.IsFalse(checkoutFormPage.errorMessageFirstName(), "FirstName error Message is showing");
                Assert.IsTrue(checkoutFormPage.errorMessageNameOnCard(), "NameOnCard error Message is not showing");
                Assert.IsTrue(checkoutFormPage.errorMessageCreditCardNumber(), "CreditCardNumber error Message is not showing");
                Assert.IsTrue(checkoutFormPage.errorMessageCvv(), "Cvv error Message is not showing");
                Assert.IsTrue(checkoutFormPage.errorMessageExpiration(), "Expiration error Message is not showing");
            });
            Console.WriteLine("Some fields is epty excluding: First Name, Last Name - testing completed");
        }

        [Test, Description("Some fields is epty excluding: First Name, Last Name, Name On Card")]
        public void SomeFieldsIsEmptyExcludingFirstNameLastNameNameOnCard()
        {
            checkoutFormPage.openCheckoutPage();
            checkoutFormPage.pageIsLoaded();
            checkoutFormPage.enterFirstName("FirstName");
            checkoutFormPage.enterLastName("LastName");
            checkoutFormPage.enterNameOnCard("NameOnCard");
            checkoutFormPage.clickContinueToCheckoutButton();
            Assert.Multiple(() =>
            {
                Assert.IsFalse(checkoutFormPage.errorMessageLastName(), "LastName error Message is showing");
                Assert.IsFalse(checkoutFormPage.errorMessageFirstName(), "FirstName error Message is showing");
                Assert.IsFalse(checkoutFormPage.errorMessageNameOnCard(), "NameOnCard error Message is showing");
                Assert.IsTrue(checkoutFormPage.errorMessageCreditCardNumber(), "CreditCardNumber error Message is not showing");
                Assert.IsTrue(checkoutFormPage.errorMessageCvv(), "Cvv error Message is not showing");
                Assert.IsTrue(checkoutFormPage.errorMessageExpiration(), "Expiration error Message is not showing");
            });
            Console.WriteLine("Some fields is epty excluding: First Name, Last Name, Name On Card - testing completed");
        }

        [Test, Description("Some fields is epty excluding: First Name, Last Name, Name On Card")]
        public void SomeFieldsIsEmptyExcludingFirstNameLastNameNameOnCardCreditCardNumber()
        {
            checkoutFormPage.openCheckoutPage();
            checkoutFormPage.pageIsLoaded();
            checkoutFormPage.enterFirstName("FirstName");
            checkoutFormPage.enterLastName("LastName");
            checkoutFormPage.enterNameOnCard("NameOnCard");
            checkoutFormPage.enterCreditCardNumber("CreditCardNumber");
            checkoutFormPage.clickContinueToCheckoutButton();
            Assert.Multiple(() =>
            {
                Assert.IsFalse(checkoutFormPage.errorMessageLastName(), "LastName error Message is showing");
                Assert.IsFalse(checkoutFormPage.errorMessageFirstName(), "FirstName error Message is showing");
                Assert.IsFalse(checkoutFormPage.errorMessageNameOnCard(), "NameOnCard error Message is showing");
                Assert.IsFalse(checkoutFormPage.errorMessageCreditCardNumber(), "CreditCardNumber error Message is showing");
                Assert.IsTrue(checkoutFormPage.errorMessageCvv(), "Cvv error Message is not showing");
                Assert.IsTrue(checkoutFormPage.errorMessageExpiration(), "Expiration error Message is not showing");
            });
            Console.WriteLine("Some fields is epty excluding: First Name, Last Name, Name On Card, Credit Card Number - testing completed");
        }

        [Test, Description("Some fields is epty excluding: First Name, Last Name, Name On Card")]
        public void SomeFieldsIsEmptyExcludingFirstNameLastNameNameOnCardCreditCardNumberCvv()
        {
            checkoutFormPage.openCheckoutPage();
            checkoutFormPage.pageIsLoaded();
            checkoutFormPage.enterFirstName("FirstName");
            checkoutFormPage.enterLastName("LastName");
            checkoutFormPage.enterNameOnCard("NameOnCard");
            checkoutFormPage.enterCreditCardNumber("CreditCardNumber");
            checkoutFormPage.enterCvv("cvv");
            checkoutFormPage.clickContinueToCheckoutButton();
            Assert.Multiple(() =>
            {
                Assert.IsFalse(checkoutFormPage.errorMessageLastName(), "LastName error Message is showing");
                Assert.IsFalse(checkoutFormPage.errorMessageFirstName(), "FirstName error Message is showing");
                Assert.IsFalse(checkoutFormPage.errorMessageNameOnCard(), "NameOnCard error Message is showing");
                Assert.IsFalse(checkoutFormPage.errorMessageCreditCardNumber(), "CreditCardNumber error Message is showing");
                Assert.IsFalse(checkoutFormPage.errorMessageCvv(), "Cvv error Message is showing");
                Assert.IsTrue(checkoutFormPage.errorMessageExpiration(), "Expiration error Message is not showing");
            });
            Console.WriteLine("Some fields is epty excluding: First Name, Last Name, Name On Card, Credit Card Number, CVV - testing completed");
        }

        [Test, Description("All Fields Are Filled")]
        public void AllFieldsFreFilled()
        {
            checkoutFormPage.openCheckoutPage();
            checkoutFormPage.pageIsLoaded();
            checkoutFormPage.enterFirstName("FirstName");
            checkoutFormPage.enterLastName("LastName");
            checkoutFormPage.enterNameOnCard("NameOnCard");
            checkoutFormPage.enterCreditCardNumber("CreditCardNumber");
            checkoutFormPage.enterCvv("cvv");
            checkoutFormPage.clickContinueToCheckoutButton();
            checkoutFormPage.enterExpiration("Expiration");
            Assert.Multiple(() =>
            {
                Assert.IsFalse(checkoutFormPage.errorMessageLastName(), "LastName error Message is showing");
                Assert.IsFalse(checkoutFormPage.errorMessageFirstName(), "FirstName error Message is showing");
                Assert.IsFalse(checkoutFormPage.errorMessageNameOnCard(), "NameOnCard error Message is showing");
                Assert.IsFalse(checkoutFormPage.errorMessageCreditCardNumber(), "CreditCardNumber error Message is showing");
                Assert.IsFalse(checkoutFormPage.errorMessageCvv(), "Cvv error Message is showing");
                Assert.IsFalse(checkoutFormPage.errorMessageExpiration(), "Expiration error Message is showing");
            });
            Console.WriteLine("All Fields Are Filled - testing completed");
        }

        [Test, Description("‘Cart’ successfully loaded when user opens the page")]
        public void CartLoaded()
        {
            checkoutFormPage.openCheckoutPage();
            Assert.IsTrue(checkoutFormPage.cartIsLoaded());
            Assert.That(checkoutFormPage.quantityOfGoods(), Is.EqualTo("2"));
            Assert.IsTrue(checkoutFormPage.countOfProductsMoreThanOne());

        }
        [Test, Description("Promo code correctly changes ‘Total’ field")]
        public void PromoCode()
        {
            checkoutFormPage.openCheckoutPage();
            Assert.IsTrue(checkoutFormPage.cartIsLoaded());
            double total = checkoutFormPage.getTotalAmountValue();
            checkoutFormPage.enterPromoCode("promo");
            checkoutFormPage.clickRedeemButton();
            Thread.Sleep(3000);
            double totalWithPromo = checkoutFormPage.getTotalAmountValue();
            Assert.IsTrue(checkoutFormPage.checkCorrectSale(total, totalWithPromo));

        }
        [Test, Description("‘Your order was placed’ displayed after form submission")]
        public void OrderWasPlaced()
        {
            checkoutFormPage.openCheckoutPage();
            Assert.IsTrue(checkoutFormPage.cartIsLoaded());
            checkoutFormPage.enterFirstName("FirstName");
            checkoutFormPage.enterLastName("LastName");
            checkoutFormPage.enterNameOnCard("NameOnCard");
            checkoutFormPage.enterExpiration("01/20");
            checkoutFormPage.enterCreditCardNumber("1111222233334444");
            checkoutFormPage.enterCvv("123");
            checkoutFormPage.enterPromoCode("promo");
            checkoutFormPage.clickRedeemButton();
            checkoutFormPage.clickContinueToCheckoutButton();
            Assert.IsTrue(checkoutFormPage.orderSuccessMessage());

        }
    }
}
