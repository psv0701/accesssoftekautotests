﻿using System;
using AccessSoftekAutoTests.RestSharpDataDriven.DataTypes;
using NUnit.Framework;
using RestSharp;
using RestSharp.Serialization.Json;


namespace AccessSoftekAutoTests.tests
{
    public class ApiTestCases
    {
        private const string BASE_URL = "https://fg1ap986e9.execute-api.eu-west-1.amazonaws.com/Dev";
        [TestCase("aaa", 3, TestName = "Discount amount is the length of coupon string = 3")]
        [TestCase("aaaaa", 5, TestName = "Discount amount is the length of coupon string = 5")]
        [TestCase("aaaaaaaaaa", 10, TestName = "Discount amount is the length of coupon string = 10")]

        public void DataFor
            (string discount, int expectedDiscount)
        {
            // arrange
            RestClient client = new RestClient(BASE_URL);
            RestRequest request =
                new RestRequest($"/coupon?coupon={discount}", Method.POST);
            // act
            IRestResponse response = client.Execute(request);
            DataResponse dataResponse = new JsonDeserializer().
                Deserialize<DataResponse>(response);
            // assert
            Assert.That(
                dataResponse.Discount,
                Is.EqualTo(expectedDiscount)
            );
        }
    }
}
